FROM ubuntu:20.04

MAINTAINER Sven Freiberg <sven.freiberg@biq.solutions>

# Silence terminal output
ENV DEBIAN_FRONTEND noninteractive

# Update source lists
RUN apt-get update -y --fix-missing

# Install dev environment.
RUN apt-get install -y -q zip unzip
RUN apt-get install -y -q git
RUN apt-get install -y -q make
RUN apt-get install -y -q jq
RUN apt-get install -y -q bc
RUN apt-get install -y -q doxygen
RUN apt-get install -y -q python3 python3-pip python3.8-venv
RUN apt-get install -y -q bash

# Change shell to bash.
RUN chsh -s /bin/bash

# Drop into shell on entry.
ENTRYPOINT /bin/bash
