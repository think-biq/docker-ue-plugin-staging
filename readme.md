# Staging tool base docker image

This image was initally intended for automation purposes in my automation pipeline to build and publish Unreal Engine plugins.
It's based on Ubuntu 20.04 and adds packages like GNU Make and python 3, as well as change the default shell to bash.
It's also used by a couple of other projects, like [piper-whistle](https://gitlab.com/think-biq).
